
## Preparing Solution

Clone Repo https://bitbucket.org/Ringo_Evan/ringo_wiki_test/
Install [maven](https://maven.apache.org/install.html)
Open a command prompt and navigate to your repo

Enter mvn test
Grab some coffee and let the test run. It's not headless and interference has caused flakiness in the past.


In your repo Navigate to ~/Target/Site/Serentity
This is where reports will be.
Due to current limitations the index report is not being generated. You will have to manually verify each file.
