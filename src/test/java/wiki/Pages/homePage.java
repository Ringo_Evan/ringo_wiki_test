/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wiki.Pages;

import java.util.List;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
/**
 *
 * @author ringoek
 * 
 * This class will handle actions and queries on the home page
 */
public class homePage extends webBasePage {
    public homePage(WebDriver driver) {
        super(driver);
    }
    
    public boolean countLanguages()
    {
      List <WebElement> topLanguages = getDriver().findElements(org.openqa.selenium.By.className("central-featured-lang"));
      return (topLanguages.size() == 10);      
    }
    
    public void searchFor(String searchTerm)
    {
        getDriver().findElement(By.id("searchInput")).sendKeys(searchTerm);
        getDriver().findElement(By.cssSelector("button[type=submit]")).click();
        
    }

}
