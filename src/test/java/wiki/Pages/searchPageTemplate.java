/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wiki.Pages;


import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
/**
 *
 * @author ringoek
 * 
 * This class will be used for various search results
 * TODO:Change to entry page?
 * TODO:Interface?
 */
public class searchPageTemplate extends webBasePage {
    
    public searchPageTemplate(WebDriver driver){
        super(driver);
    }

    //Check that we are not on bad search result
    //ToDo: REfactor to fit with WIKI stub style instead of based on searching
    public boolean validSearch() {
        return (getDriver().findElements(By.id("ca-nstab-special")).size() <= 0);
        
    }
    
    
}
