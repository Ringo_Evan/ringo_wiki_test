/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wiki.Pages;

/**
 *
 * @author ringoek
 */


import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.*;

import java.util.List;
import java.util.concurrent.TimeUnit;

//base class for page objects
public class webBasePage extends PageObject {

    private WebDriverWait webDriverWait;

    //set wait times and launch driver
    public webBasePage(WebDriver driver) {
        super(driver);
        //String curPath = System.getProperty("user.dir");
        //String chromePath = curPath + "\\libs\\chromedriver\\chromedriver.exe";
        //System.setProperty("webdriver.chrome.driver", chromePath);
        webDriverWait = new WebDriverWait(driver, 30);
        //driver.manage().window().maximize();
    }

    
    public String getCurrentPageUrl() {
        return getDriver().getCurrentUrl();
    }
    
    
    public void maximizeWindow() {
        getDriver().manage().window().maximize();
    }

    //TODO:Check into what text element pulls, so can use on slower loading pages
    public void waitForTextValueToBeAppeared(WebElement element, String text) {
        
        webDriverWait.until(ExpectedConditions.textToBePresentInElementValue(element, text));
    }
   
}
