/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserStoryWiki;

import Steps.WikiSteps;
import java.util.Arrays;
import java.util.Collection;
import org.openqa.selenium.WebDriver;
import net.thucydides.core.annotations.*;
import net.serenitybdd.junit.runners.SerenityParameterizedRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import net.thucydides.junit.annotations.TestData;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 *
 * @author ringoek
 * 
 * This class is used to run the any tests we want to verify searching
 */

@RunWith(SerenityParameterizedRunner.class)
public class searchTestingFeatureTest {
   //@Managed(driver="chrome")
    
    //Closes browsers between test runs(including each run with diffenerent data)
    @Managed(uniqueSession=true)
    private static WebDriver gChromeDriver;
    
    
    private String searchString;
    
    //set up which search strings we want to run against
    @TestData
     public static Collection< Object[] > testData(){
        return Arrays.asList(new Object[][]{
            {"Test"},
            {"France"}
        });
    }
     
    @Steps
    //our user stories will explain what Jon is doing when he searches
    private WikiSteps Jon;
     
    //The class will be called once for each entry in the test data collection
    //Then set up the search string to be used
    public searchTestingFeatureTest(String searchString) {
        this.searchString = searchString;
    }
   
   
    @Test
    //This test will  test will check that searches are not getting a bad result, then verify the URL
    public void wikiSerachTest()
    {
        //Given
        Jon.launchesWiki();
        //When
        Jon.searchesFor(searchString);
        //Then
        Jon.doesNotSeeSearchResults();
        //And
        Jon.isAt("https://en.wikipedia.org/wiki/" + searchString);
        
        
    }
}

