/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserStoryWiki;


import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;



import org.openqa.selenium.WebDriver;
import net.thucydides.core.annotations.*;
import net.thucydides.core.annotations.Steps;

import Steps.*;
import net.serenitybdd.junit.runners.SerenityRunner;
import org.junit.runner.RunWith;

/**
 *
 * @author ringoek
 * 
 * Main test Class. Handles 2 of 3 basic test cases. 
 * TODO: abstract out browser type instead of using default FireFox
 * TODO: Fix Reporting
 * TODO: Write Top 10 Languages to a file
 * TODO: Implement scenario and feature
 * TODO: Rename as Stories
 */


@RunWith(SerenityRunner.class)
public class MonsterTest {
    //closes the browser each test
    @Managed(uniqueSession=true)
    private static WebDriver gChromeDriver;
   
    public MonsterTest(){
        
        //wikiSteps = new WikiSteps();
    }
    
    @Steps
    //our user stories involve Jon accessing the website
    private WikiSteps Jon;
    
    
    @BeforeClass
    //don't need to use this anymore with Serentiy 
    public static void setUpClass() {
        
        
        //String curPath = System.getProperty("user.dir");
        //String chromePath = curPath + "\\libs\\chromedriver\\chromedriver.exe";
        //System.setProperty("webdriver.chrome.driver", chromePath);
        //gChromeDriver = new ChromeDriver();
        
    }
    
    @AfterClass
    //nothing worth clearning up
    public static void tearDownClass() {
    }
    
    @Test
    //This test will  verify the URL when Jon goes to Wikipedia. 
    public void wikiHome()
    {
        //When 
        Jon.launchesWiki();
        //Then       
        Jon.isAt("https://www.wikipedia.org/");
        
    
    }
    
    
    @Test
    //This test will verify that 10 languages upon loading Wiki
    public void wikiLanguageCheck()
    {
        //When
        Jon.launchesWiki();
       
        //Then 
        Jon.seesTenLanguages();
       
    
    }

    @Before
    //nothing to set up
    public void setUp() {
        //gChromeDriver.get("https:\\www.google.com");
    }
    
    @After
    //nothing to clean up
    public void tearDown() {
    }

   
}
