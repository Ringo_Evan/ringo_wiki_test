/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Steps;

import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import static org.junit.Assert.*;
import org.openqa.selenium.WebDriver;

import net.serenitybdd.core.annotations.findby.By;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;
import org.openqa.selenium.WebElement;

import wiki.Pages.homePage;
import wiki.Pages.searchPageTemplate;
/**
 *
 * @author ringoek
 * 
 * This class provides all the basic steps for using Wiki
 * TODO split class into multiple steps based on different funcitons
 */
public class WikiSteps extends ScenarioSteps {
    
    //create page objects for interation with
     private homePage mainPage;
     private searchPageTemplate searchPage;
    
    public WikiSteps() {
    }
    
       
    @Step
    //This step will launch a broweser and go to Wiki
    //It checks for the logo at the top of the page
    public void launchesWiki(){
       
      getDriver().get("https://www.wikipedia.org");
      mainPage.maximizeWindow();
      String test = getDriver().findElement(By.className("central-textlogo")).getText();
      assertEquals(test, "Wikipedia\nThe Free Encyclopedia");
      //mainPage.waitForTextValueToBeAppeared(getDriver().findElement(By.className("central-textlogo")), "Wikipedia\n" + "The Free Encyclopedia");
    
    }
    
    @Step
    //This step will enter a search term and presses goo
    //TODO make this generic so it works from any page, not just main page?
    public void searchesFor(String sSearch){

        //getDriver().findElement(By.xpath("//*[@id=\"searchInput\"]")).sendKeys("Sending");
        mainPage.searchFor(sSearch);     
    
    }
    
    @Step
    //This step will check what URL the user is att
    public void isAt(String testURL)
    {
        String url = getDriver().getCurrentUrl();
        assertEquals(url, testURL);
    
    }

    
    @Step
    //This step checks there are 10 visible languages
    public void seesTenLanguages() {
        
        assertTrue(mainPage.countLanguages());
        
    }

    @Step
    //This step will check that we a search result has not yielded an invalid search
    public void doesNotSeeSearchResults() {
       assertTrue(searchPage.validSearch());        
       
    }
}
